import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { Reponse } from '../types';

@Component({
  selector: 'app-reponse',
  templateUrl: './reponse.component.html',
  styleUrls: ['./reponse.component.scss']
})
export class ReponseComponent implements OnInit, OnChanges {
  /** bonne réponse choisi, null si la réponse n'a pas encore été validé */
  @Input() bonneReponse?:number=null;

  /**Reponses possibles */
  @Input() reponses: Reponse[] = [];

  @Input() allGuess: Map<number, string[]> = new Map();

  /**Event, quand le joueur selectionne une réponse (idReponse)*/
  @Output() guess : EventEmitter<string> = new EventEmitter();

  /**Id de la réponse séléctionnée*/
  public reponseChoisi?:number = null;

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    /**Quand on charge une nouvelle question, on clear la séléction précédente */
    if(changes.reponses){
      this.bonneReponse = null;
      this.reponseChoisi = null;
      this.allGuess = new Map();
    }
  }

  ngOnInit(): void {

  }

  /**
   * Quand le joueur clique sur une réponse
   * @param idReponse ID de la réponse séléctionnée
   */
  onGuess(idReponse){
    if(!this.reponseChoisi){
      this.reponseChoisi = idReponse;
      this.guess.next(idReponse);
    }
  }



}
