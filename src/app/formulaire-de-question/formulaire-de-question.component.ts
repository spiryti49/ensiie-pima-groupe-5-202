import { Component, OnInit } from '@angular/core';
import { QuizzAPIService } from '../../services/QuizzAPI.service';
import { AskAndAnswer, Question, Reponse } from '../types';

import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/services/Auth.service';

@Component({
  selector: 'app-formulaire-de-question',
  templateUrl: './formulaire-de-question.component.html',
  styleUrls: ['./formulaire-de-question.component.scss']
})
export class FormulaireDeQuestionComponent implements OnInit {

  constructor(private apiservice:QuizzAPIService, private activatedRoute: ActivatedRoute, private authService : AuthService, private router: Router) { }
  public question:Question={idQuestion:0,question:"ceci est une questino de tests destinée à être très très très très très très très lonnnnnnnnnnnnnnnnnnngue"};
  /** reponses possibles chargé sur la page*/
  public reponses:Reponse[]=[{idReponse:1,reponse:"test1"},{idReponse:2,reponse:"test2"},{idReponse:3,reponse:"test3"}, {idReponse:4, reponse:"test4"}];
  /**ensemble des questions posé */
  public questionPose:number[]=[];
  /** score pour la question en cours */
  public score:number=0;
  /** indique le moment où la question apparaît */
  public starttime:number;
  /** indique le moment où la réponse est donnée */
  public endtime:number;
  /**Indique si la réponse est correcte ou non */
  public estCorrect: boolean = null;
  /** bonne réponse choisi, null si la réponse n'a pas encore été validé */
  public bonneReponse?:number=null;
  /**le temps total du mode jeu */
  public fulltime:number=100;
  /**interval pour calculer le temps */
  public interval=null;

  public isEnd:boolean=false;

  public nbrQuestion:number=20;

  public isTimerMode:boolean=false;
  
  /**
   * récupère une question de la base et la stock dans les variables de classe question et reponses de la classe et calcul le starttime
   */
  ngOnInit(): void {
    if(this.authService.isAuth()){
      this.activatedRoute.queryParams.subscribe(params => {
        this.isTimerMode = params.timer_mode;
        this.setup();
      });
    }else{
      this.router.navigate(['authentification']);
    }
    
  }

  setup(){
    this.score = 0;
    this.stopInterval();
    this.isEnd = false;
    this.nbrQuestion = 20;
    this.fulltime = 100;
    this.questionPose = [];

    this.apiservice.getQuestion(this.questionPose).subscribe((data:AskAndAnswer)=>
    {
      this.question=data.question;
      this.reponses=data.reponses;

      if(this.isTimerMode){ 
        this.startInterval();
      }else{
        this.starttime= new Date().getTime();
      }
    })
  }

  sendReponse(reponseChoisi){

    this.endtime= new Date().getTime();
    this.apiservice.sendReponse(this.question.idQuestion,reponseChoisi).subscribe((data:{idReponse:number})=> {
      const bonneReponse=this.reponses.find(element => element.idReponse===data.idReponse);
      if (bonneReponse!=undefined){
        this.bonneReponse=bonneReponse.idReponse;
      }
      else{
        console.warn("problème de recherche")
      }
      this.estCorrect= reponseChoisi===data.idReponse;
      if (this.estCorrect){
        if(this.isTimerMode){
          this.score+=1;
        }else{
          let answertime= Math.round((this.endtime - this.starttime)/1000); /**Le temps mis à répondre en seconde */
          if (answertime < 20){
            this.score += 20 - answertime;
          }
          else{
            this.score += 0;
          }
        }
      }
    })
  }
  nextQuestion(){
    this.bonneReponse=null;
    if(this.nbrQuestion<=1){
      this.isEnd = true;
    }
    else{
      this.getOtherQuestion();
    }
  }

  async getOtherQuestion(){
    this.questionPose.push(this.question.idQuestion);
    if (this.questionPose.length==8){
      this.questionPose=[];
    }
    let verif=true;
    while(verif){
      let data:AskAndAnswer= await this.apiservice.getQuestion(this.questionPose).toPromise();
      verif=this.questionPose.includes(data.question.idQuestion);
      if (! verif){
        this.question=data.question;
        this.reponses=data.reponses;

        if(!this.isTimerMode){ 
          this.nbrQuestion -= 1;
          this.starttime= new Date().getTime();
        }
      }
    }

  }
  startInterval(){

    if(this.interval){
      this.stopInterval();
    }
    this.interval=setInterval(()=>this.runInterval(),1000);
  }
  stopInterval(){
    if(this.interval){
      clearInterval(this.interval);
      this.interval=null;
    }
  }
  runInterval(){
    if (this.fulltime>0){
      this.fulltime-=1;
      
    }
    else{
      this.stopInterval();
      this.isEnd = true;
    }
  }
}

