/**
 * Utilitaires
 */
class Utils {
    /**
 * Genere un ID random en string
 * @param length Taille de l'id
 * @returns Random string id
 */
    static randomID(length) {
        return [...Array(length)].map(() => Math.floor(Math.random() * 16).toString(16)).join('');
    }
}

module.exports = Utils;

