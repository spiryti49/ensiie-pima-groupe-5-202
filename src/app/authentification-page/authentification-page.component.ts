import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/services/Auth.service';

@Component({
  selector: 'app-authentification-page',
  templateUrl: './authentification-page.component.html',
  styleUrls: ['./authentification-page.component.scss']
})
export class AuthentificationPageComponent implements OnInit{


  /**Element HTML <input> correspondant au pseudo */
  @ViewChild('username')
  username!: ElementRef;

  /**Le Pseudo est il valide ? */
  isValid = false;

  /**URL de redirection, après être log */
  callbackURL:string = 'home';

  /**Message d'erreur, null si vide */
  error:string = null;

  constructor(private router: Router, private authService: AuthService,private activatedRoute:ActivatedRoute) { }

  ngOnInit(): void {
    this.authService.logout();
    
    this.activatedRoute.queryParams.subscribe(params => {
      if(params.callback){
        this.callbackURL = params.callback;
      }
    })
  }

  /**Quand le joueur clique sur jouer */
  onPlay(){
    this.check();
    if(this.isValid){
      /**On enregistre son pseudo dans le service d'authentification */
      this.authService.login(this.username.nativeElement.value);
      /**On redirige vers l'URL de redirection*/
      this.router.navigate([this.callbackURL]);
    }
  }

  /**Fonction pour check si le pseudo est valide */
  check(){
    this.isValid = /^([A-z0-9_]){3,15}$/.test(this.username.nativeElement.value); /**Regexp : Combinaison de chiffres, lettres, _, de taille >=3 && <= 15 */
    this.error = null;
    if(!this.isValid){
      this.error = 'Votre pseudo doit faire entre 3 et 15 caractères et doit comprendre seulement des lettres, chiffres,et _';      
    }
  }
}
