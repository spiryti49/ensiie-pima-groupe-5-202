



export type Question ={
    idQuestion:number,
    question:string,
};

export type Reponse = {
    idReponse:number,
    reponse:string,
};
export type AskAndAnswer = {
    question:Question,
    reponses:Reponse[],
}; 

export type PlayerScore = {
    username: string,
    score: number
};

export type QuotetoAdd = {
    quote:string,
    answer:string,
};

/**
 * Type représentant un Joueur (multiplayer)
 */
export type Player = {
    username:string,
    id:string,
    score:number,
    guessed?:boolean
};

/**
 * Etat d'une room:
 * CONFIGURING : Configuration et attente des joueurs
 * PLAYING: En jeu
 * END: Fin du jeu, affichage des scores
 */
export enum RoomState{
    CONFIGURING="CONFIGURING", PLAYING="PLAYING", END="END"  
};