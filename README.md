


npm install -g angular-cli

npm install -g nodemon

npm install -g npm@6

npm install




------------------------------------------------

ng serve => démarre un serveur frontend

ng build => build le frontend


nodemon server.js => demarre le backend

--------------------------------------
mettre en place la base de données pour faire des tests:

-installer mariaDb:  https://mariadb.org/download/

-ouvrir le terminal Mariadb (barre de recherche sur windows rechercher mariadb)

-dans le terminal se placer dans le répertoire /User/"nom utilisateur"/Onedrive/Bureau/..../backend/database avec cd

-se connecter à la base avec cette commande:
mysql -u "nom utilisateur choisi lors de la création de la base" -p 

-rentrer le password choisi lors de la création de la base

-Créer la database:
Create Database QuIIzE;

-se connecter dessu: 
Use QuIIzE;

-effectuer ces commande:

```
\. Tables.sql
\. Import.sql

```

-Dans le fichier database du backend créer un fichier mysql_conf.json
rentrer ces données:

{
    "host":"localhost",
    "user":"user choisi pour la bdd",
    "password":"password choisi pour la bbdd",
    "database":"quIIzE"
}

------------------------------------------------------------------

