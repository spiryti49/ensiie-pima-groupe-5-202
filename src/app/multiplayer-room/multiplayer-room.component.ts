import mdiShieldCheckOutline from '@iconify/icons-mdi/shield-check-outline';

import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/services/Auth.service';
import { MultiplayerService } from 'src/services/Multiplayer.service';
import { randomID } from 'src/app/Utils';
import { Player, PlayerScore, Question, Reponse, RoomState } from '../types';


@Component({
  selector: 'app-multiplayer-room',
  templateUrl: './multiplayer-room.component.html',
  styleUrls: ['./multiplayer-room.component.scss']
})
/**
 * Component pour une room multijoueur
 */
export class MultiplayerRoomComponent implements OnInit, OnDestroy {
  /**Icone administrateur */
  adminIcon = mdiShieldCheckOutline;

  /**Enumeration des Etats (pour être utilisé dans .html) */
  roomStateEnum = RoomState;
  
  /**True si la room est en chargement */
  isLoading = true;
  
  /**True si le lien d'invitation à été clické */
  isInviteClicked = false;

  /**ID de l'administrateur de la room */
  adminID?:string;

  /**True si l'utilisateur actuel est admin dans la room */
  isAdmin = false;

  /**Etat de la room */
  roomState: RoomState = RoomState.CONFIGURING;
  
  /**ID de la room */
  idRoom?:string;

  /**Score max de la room */
  maxScore:number = 10;

  /**Scoreboard des joueurs*/
  scoreboard : Player[] = [];

  /* Deep copy from scoreboard with only username and score */
  scoreboardEnd : PlayerScore[] = [];

  /**Input Range pour le score max de la room */
  @ViewChild('maxScoreInput')
  maxScoreInput:ElementRef;

  /**ID de la bonne réponse */
  bonneReponse:number = null;

  /**Liste des reponses possibles */
  reponses:Reponse[]=[];

  /**Question actuelle */
  question:Question = {question:"", idQuestion:0};

  /**Message d'erreur (null si rien) */
  errorMessage:string = null;

  /**
   * Toutes les reponses des joueurs sur la question actuelle
  */
  allGuess: Map<number, string[]> = new Map();
  
  constructor(private multiplayerService :MultiplayerService, private authService :AuthService, private router:Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {    
    /**On recup l'id de la room via l'url : /room/<id> */
    this.activatedRoute.params.subscribe(params => {
      /**Si le joueur n'est pas login, on redirige vers le formulaire de connexion, en précisant de revenir sur cette url après la connexion*/
      if(!this.authService.isAuth()){
        const idRoom = params.id ? params.id : '';
        this.router.navigate(['authentification'], {queryParams: { callback: `/room/${idRoom}`}});
      }else{
        if(params.id){
          /*Si l'id de la room est dans l'URL*/
          this.initRoom(params.id);
        }else{
          /**Si aucun ID, on génére un ID */
          this.router.navigate([`/room/${randomID(5)}`]);
        }
      }
    });
  }

  ngOnDestroy():void{
    this.multiplayerService.logout();
  }

  /**
   * Initialise la room à rejoindre
   * @param idRoom ID de la room à rejoindre
   */
  initRoom(idRoom:string):void{
    this.idRoom = idRoom;
    /**Connection au serveur multijoueur */
    this.multiplayerService.connect();

    /**Demande à rejoindre la room */
    this.multiplayerService.requestRoom(idRoom);

    /**On écoute les evenements */
    this.multiplayerService.onJoiningRoom.subscribe(this.handleJoinRoom.bind(this));
    this.multiplayerService.onUserChange.subscribe(this.handleUserChange.bind(this));
    this.multiplayerService.onAdminChange.subscribe(this.handleAdminChange.bind(this));
    this.multiplayerService.onConfigurationChange.subscribe(this.handleConfigurationChange.bind(this));
    this.multiplayerService.onStateChange.subscribe(this.handleStateChange.bind(this));
    this.multiplayerService.onNewQuestion.subscribe(this.handleNewQuestion.bind(this));
    this.multiplayerService.onPlayerGuessed.subscribe(this.handlePlayerGuessed.bind(this));
    this.multiplayerService.onReponse.subscribe(this.handleReponse.bind(this));
    this.multiplayerService.onScoreboard.subscribe(this.handleScoreboard.bind(this));
    this.multiplayerService.onAllGuess.subscribe(this.handleAllGuess.bind(this));
  }

  /**
   * Event: On rejoin une room 
   * @param event : {id: ID de la room, error: Message d'erreur si on a pas pu rejoindre la room}
   */
  handleJoinRoom(event){
    if(event.id){
      this.isLoading = false;
    }else if(event.error){ /**Error */
      this.errorMessage = event.error;
    }
  }

  /**
   * Event: Des joueurs ont rejoint la room, ou un joueur à quitté
   * @param event {user_join: [{username, id}], user_left:id}
   */
  handleUserChange(event){
    /**Des joueurs ont rejoint */
    if(event.user_join){
      for(const user of event.user_join){
        /**On ajoute les nouveaux joueurs*/
        this.scoreboard.push({username: user.username, id: user.id, score:0});
      }
    }
    /**Un joueur à quitté */
    if(event.user_left){
      const index = this.scoreboard.findIndex(p => p.id === event.user_left);
      this.scoreboard.splice(index, 1);
    }
  }

  /**
   * Event: changement du joueur admin de la room
   * @param event {adminID: id}
   */
  handleAdminChange(event){
    this.adminID = event.admin;
    /**L'utilisateur actuel est-il admin ? */
    this.isAdmin = (this.adminID === this.multiplayerService.getPlayerID()); 
  }

  /**
   * Event: changement de configuration de la room
   * @param event {max_score: number}
   */
  handleConfigurationChange(event){
    this.maxScore = event.max_score;
    /**Mis à jour de l'input 'range' */
    this.maxScoreInput.nativeElement.value = this.maxScore;
  }

  /**
   * Quand l'état de la room change
   * @param state :RoomState
   */
  handleStateChange(state: RoomState){
    this.roomState = state;

    if(this.roomState === RoomState.END){
      this.scoreboardEnd = this.scoreboard.map(player => {return {username : player.username, score: player.score}});
    }

    if(this.roomState === RoomState.CONFIGURING){
      for(const player of this.scoreboard){
        player.score = 0;
        player.guessed = false;
        this.isInviteClicked = false;
      }
    }
  }

  /**
   * Recoi une nouvelle question et ses réponses
   * @param question 
   */
  handleNewQuestion(question){
    this.bonneReponse = null;
    
    for(const player of this.scoreboard){
      player.guessed = false;
    }

    this.question = question.question;
    this.reponses = question.reponses;
  }

  /**
   * Quand un joueur a validé sa réponse
   * @param idPlayer ID du joueur qui a joué
   */
  handlePlayerGuessed(idPlayer : string){
    this.scoreboard.find(p=>p.id === idPlayer).guessed = true;
  }

  /**
   * Quand on recoit la bonne réponse
   * @param idReponse ID de la bonne réponse recu
   */
  handleReponse(idReponse : number){
    this.bonneReponse = idReponse;
  }

  /**
   * 
   * @param players Liste des id <> score
   */
  handleScoreboard(scoreboard){
    for(const score of scoreboard){
      this.scoreboard.find(p=> p.id === score.player).score = score.score;
    }
    this.updateScoreboard();
  }

  /**
   * 
   * @param allGuess Liste des reponses par joueurs
   */
  handleAllGuess(allGuess){
    this.allGuess = new Map();
    for(const guess of allGuess){
      let liste = this.allGuess.has(guess.reponse) ? this.allGuess.get(guess.reponse) : [];
      liste.push(this.scoreboard.find(p => p.id === guess.player).username );
      this.allGuess.set(guess.reponse, liste);
    }
  }

  /**
   * Update scoreboard
   */
  updateScoreboard(){
    this.scoreboard = this.scoreboard.sort((pa, pb)=>{return pb.score - pa.score});
  }

  /**
   * Quand le joueur clique sur le bouton 'Invite'
   * On copie le lien dans le press-papier
   */
  onInviteClick(){
    navigator.clipboard.writeText(`${window.location.host}/room/${this.idRoom}`).then(()=>{
      /**A cliqué sur le boutton */
      this.isInviteClicked = true;
    });
  }

  /**
   * Quand le joueur clique sur lancer la partie
   */
  onStartClick(){
    this.multiplayerService.startRoom();
  }

  /**
   * Quand le joueur propose une réponse
   * @param reponseID réponse du joueur
   */
  onGuessClick(reponseID : number) {
    this.multiplayerService.guess(reponseID);
  }

  /**
   * Quand le joueur change la valeur du max score
   * @param event 
   */
  onChangeMaxScore(event){
    this.maxScore = parseInt(event.target.value);
    this.multiplayerService.changeRoomConfiguration(this.maxScore);
  }

  /**
   * Quand le joueur veut rejouer
   */
  onReStartClick(){
    this.multiplayerService.restartGame();
  }

}
