
const database = require('../database/database');

const RoomState = {
    CONFIGURING:"CONFIGURING", PLAYING:"PLAYING", END:"END"  
};

/**
 * Cette classe représente une room (une partie multijoueur)
 */
class Room {
    constructor(id, multiPlayerManager) {
        this.id = id; /**ID de la room */
        this.state = RoomState.CONFIGURING; /**True si la partie est commencée */
        this.players = new Map(); /**Map contenant les joueurs de la partie : IDJoueur -> Joueur */
        this.admin = null; /**Admin de la room */
        this.multiPlayerManager = multiPlayerManager;
        this.maxScore = 10; /**Score maximum à atteindre */
        this.idCurrentQuestion = null; /**Id de la question actuelle */
        this.guessedPlayer = []; /**Liste des joueurs ayant répondu à la question {player:id, reponse:id} */
        this.skipQuestionTimeout = null; /**Timeout pour passer à la question suivante */
    }

    /**
     * Retourne l'id de la Room
     * @returns ID de la Room (string)
     */
    getID() {
        return this.id;
    }

    /**
     * Ajoute un joueur à la room
     * @param {*} player le joueur qui essaye de rejoindre
     * TODO : Renvoyer un message d'erreur si la partie à déjà commencée
     */
    join(player) {
        //Ne peut pas join si la partie est déjà lancée
        if(this.state === RoomState.PLAYING){
            player.send({room:{error:"Partie déjà en lancée !"}});
            return;
        }

        //Ne peut pas join si un joueur à le même pseudo
        if(Array.from(this.players.values()).find(p=>p.username===player.username)){
            player.send({room:{error:"Un joueur possède déjà ce pseudo dans cette partie."}});
            return;
        }

        player.setRoom(this); /**On ajoute la room au joueur */
        player.send({ room: { id: this.id } }); /**On envoit un message 'room' au joueur pour confirmer qu'il a pu entrer dans cette room */
        this.broadcast({ user_join: [{ id: player.id, username: player.username }] }); /**On broadcast un message 'user_join' à tous les joueurs déjà présents */
        this.players.set(player.id, player); /**On ajoute le joueur à la liste des joueurs de la room */
        const allPlayers = Array.from(this.players.values()).map(function (p) { return { username: p.username, id: p.id } }); /**On récupere tous les joueurs présents dans la room */

        player.send({ user_join: allPlayers }); /**On envoit tous les joueurs présent au nouveau joueur */
        player.send({room_configuration:{max_score:this.maxScore}}); /**On envoit les configurations au joueur qui vient de rejoindre */
        /**Si il y a un admin, on l'envoi au joueur qui vient de rejoindre */
        if(this.admin){
            player.send({admin:this.admin.id});
        }


        this.updateAdmin();
    }

    /**
     * Fait quitter un joueur de la room
     * @param {*} id ID du joueur à exclure de la room
     */
    left(id) {
        if(!this.players.has(id)){
            return;
        }
        this.players.delete(id); /**Retire le joueur de la liste */
        this.broadcast({ user_left: id }); /**Broadcast un message 'user_left' aux joueurs */
        this.updateAdmin();
        if(this.state === RoomState.PLAYING) this.sendCorrectionIfNeeded();
    }

    /**
     * Update l'admin de la room
     */
    updateAdmin(){
        if(!this.players){
            return;
        }
        /**Si l'admin actuel est toujours dans la room, on ne le change pas */
        if(this.admin && this.players.has(this.admin.id)){
            return;
        }

        const playersArr = Array.from(this.players.values());
        if(playersArr.length > 0){
            /**On prend un joueur et on le passe admin */
            const player = playersArr[0];
            this.admin = player;
            this.broadcast({ admin:this.admin.id}); /**On envoi un message à tout le monde pour notifier le changement d'admin */
        }else{
            /**Si il n'y a plus de joueur dans la room, on ferme la room */
            this.destroy();
        }
    }

    /**
     * Quand un joueur demande à changer les parametres de la room
     * @param {*} fromPlayerID ID du joueur qui effectu la demande
     * @param {*} configuration Configuration de la room
     */
    changeConfiguration(fromPlayerID, configuration){
        if(this.state !== RoomState.CONFIGURING){
            return;
        }

        const newMaxScore = configuration.max_score;
        /**Si la conf est valide */
        if(Number.isInteger(newMaxScore) && newMaxScore >= 5 && newMaxScore <= 50){
            /**Si le qui effectu la demande est admin de la room */
            if(this.admin.id === fromPlayerID){
                this.maxScore = newMaxScore;
                /**On envoit un message de changement de configuration à tout le monde, sauf au joueur qui a fait la demande */
                const otherPlayers = Array.from(this.players.values()).filter(p => p.id !== fromPlayerID);
                this.broadcastList({room_configuration:{max_score:this.maxScore}}, otherPlayers);
            }
        }
    }

    /**
     * Demande à une room de démarrer le jeu
     * @param {*} fromPlayerID ID du joueur qui fait la demande
     */
    start(fromPlayerID){  
        if(this.admin.id === fromPlayerID && this.state !== RoomState.PLAYING){
            this.state = RoomState.PLAYING;
            this.broadcast({room_state: RoomState.PLAYING});
            this.sendNextQuestion();
        }
    }

    /**
     * Demande à la room de restart, affichage de panneau de configuration
     * @param {*} fromPlayerID ID du joueur qui fait la demande
     */
    restart(fromPlayerID){
        if(this.admin.id === fromPlayerID){
            this.state = RoomState.CONFIGURING;
            this.broadcast({room_state: RoomState.CONFIGURING});
            this.idCurrentQuestion = null;
            this.guessedPlayer = [];
            for(const [id, player] of this.players){
                player.score = 0;
            }
        }
    }

    /**
     * When a player guess
     * @param {*} fromPlayerID Who is guessing
     * @param {*} reponseID ID de la réponse envoyé
     */
    guess(fromPlayerID, reponseID){
        if(!this.players.has(fromPlayerID)){
            return;
        }
        if(!this.guessedPlayer.find(p => p.player === fromPlayerID)){
            this.guessedPlayer.push({player:fromPlayerID, reponse:reponseID}); /**Ajout du joueur dans une liste des joueurs qui ont deviné */
            this.broadcast({player_guessed: fromPlayerID}); /**Envoi d'un message à tout le monde pour notifier que le joueur à répondu */
            this.sendCorrectionIfNeeded();
        }
        
    }


    /**
     * Calcul du score de chaque joueur en fonction de la réponse
     * @param {*} idCorrectReponse ID de la réponse correct
     */
    updateScore(idCorrectReponse){
        for(const guess of this.guessedPlayer){
            if(guess.reponse === idCorrectReponse){
                const player = this.players.get(guess.player);
                player.score += 1;
            }
        }
    }

    /**
     * Savoir si le score max a été atteint ou non
     * @returns True si le score max à été atteint
     */
    isMaxScoreReached(){
        return Array.from(this.players.values()).find(p => p.score >= this.maxScore) !== undefined;
    }

    /**
     * Envoi des réponse de chacun à tout le monde
     */
    sendAllGuess(){
        this.broadcast({all_guess: this.guessedPlayer});
    }

    /**
     *Si tout le monde à répondu, on envoit la bonne réponse, puis on passe à la question suivante après un petit délai
     * @param {*} force Force l'envoie de la correction
     */
    sendCorrectionIfNeeded(force=false){
        if((this.guessedPlayer.length >= this.players.size) || force){
            clearTimeout(this.skipQuestionTimeout); //on stop le skip question timeout
            database.getCorrectAnswer(this.idCurrentQuestion, (resultat)=>{
                this.updateScore(resultat.idReponse);
                this.broadcast({reponse:resultat.idReponse});
                this.sendAllGuess();
                setTimeout(()=>{
                    if(this.players){ /**Si il reste des joueurs */
                        this.sendScoreboard(); /**Envoi des scores */
                        if(this.isMaxScoreReached()){ /**Si le score max à été atteint */
                            this.endGame(); /**On termine le jeu */
                        }else{ /**Sinon, on passe à la question suivante */
                            this.sendNextQuestion();
                        }
                    }
                
                }, 4500); /**On envoit la prochaine question après 3s */
            }, ()=>{
                console.error("[Multijoueur]REPONSE ERROR DB");
            });
        }
    }

    /**
     * Fin de jeu, envoi du scoreboard
     */
    endGame(){
        this.broadcast({room_state: RoomState.END});
    }

    /**
     * Envoi des scores des joueurs
     */
    sendScoreboard(){
        const scoreboard = Array.from(this.players.values()).map(player => {return {player: player.id, score: player.score}});
        this.broadcast({scoreboard: scoreboard});
    }

    /**
     * Envoi la prochaine question, et un ensemble de réponses aux joueurs
     */
    sendNextQuestion(){
        this.guessedPlayer = [];
        database.getRandomQuestionReponses((question)=>{
            this.idCurrentQuestion = question.question.idQuestion;
            this.broadcast({question:question});
        },
        ()=>{
            console.error("[Multijoueur]QUESTION ERROR DB");
        });

        /**Dans 10s on force le passage à une autre question */
        this.skipQuestionTimeout = setTimeout(()=>{
            this.sendCorrectionIfNeeded(true);
        }, 10000);
    }

    /**
     * Envoit un message à tous les joueurs de la room
     * @param {*} message le message à envoyer
     */
    broadcast(message) {
        for (const [id, player] of this.players) {
            player.send(message);
        }
    }

    /**
     * Broadcast à une liste de joueur
     * @param {*} message Message à envoyer
     * @param {*} list Liste de joueur à qui le message va être envoyé
     */
    broadcastList(message, list){
        list.map(p=>p.send(message));
    }

    /**
     * Ferme la room
     */
    destroy(){
        this.multiPlayerManager.removeRoom(this.id);
        this.id = null;
        this.players = null;
        this.multiPlayerManager = null;
        this.state = null;
        if(this.skipQuestionTimeout) clearTimeout(this.skipQuestionTimeout);
    }

    
}

module.exports = Room;