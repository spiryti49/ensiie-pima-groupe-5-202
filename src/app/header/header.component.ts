import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/services/Auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  username:string = null;
  constructor(private usernameService:AuthService) { }

  ngOnInit(): void {
    this.usernameService.onLog.subscribe(newUsername=>{
      this.username=newUsername
    });
    
  }

}
