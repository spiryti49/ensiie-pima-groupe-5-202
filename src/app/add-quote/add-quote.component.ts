import { Component, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QuizzAPIService } from 'src/services/QuizzAPI.service';
import { QuotetoAdd } from '../types';

@Component({
  selector: 'app-add-quote',
  templateUrl: './add-quote.component.html',
  styleUrls: ['./add-quote.component.scss']
})
export class AddQuoteComponent implements OnInit {
  form: FormGroup;

  currentQuote: QuotetoAdd = {quote:'', answer:''};

  message: string = null;

  constructor(private apiservice: QuizzAPIService,public fb:FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      quote: [this.currentQuote.quote, Validators.required],
      answer: [this.currentQuote.answer, Validators.required],
    });
  }

  addQuote(){
    const newQuote : QuotetoAdd = {quote: this.form.get('quote')?.value, answer: this.form.get('answer')?.value};
    if(newQuote.quote && newQuote.answer && newQuote.quote.length > 5  && newQuote.answer.length > 5){
      this.apiservice.sendQuote(newQuote).subscribe(()=>{
        this.message = "Citation ajoutée !";
        this.form.setValue({
          quote: '', 
          answer: ''
        });        
      });
    }else{
      this.message = 'Essayez quelque chose de plus complet...';
    }
  }

}
