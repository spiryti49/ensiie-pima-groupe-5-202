const express = require('express');
const path = require('path');
const app = express();

const port = 1443;


const MultiPlayerManager = require('./backend/multiplayer/MultiplayerManager');

/** Lance le serveur WebSocket pour le multijoueur */
MultiPlayerManager.start();


/** Port perso pour anunes.iiens.net  1443 */

const posts = require('./backend/routes/api');
app.use(express.json());

app.use(express.static(path.join((__dirname, 'dist/projet-pima'))));

app.use('/api', posts);

app.get('*', (req, res)=>{
  res.sendFile(path.join(__dirname, 'dist/projet-pima/index.html'));
});

app.listen(port, ()=>{
	console.log(`Server :${port}`);
});




