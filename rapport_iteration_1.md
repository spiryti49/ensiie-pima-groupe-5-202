# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | Gabriel Reboul           |
| **Scrum Master**        | Adrien Nunes            |

## Bilan de l'itération précédente  
### Évènements 
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*
> -Nous avons organiser une mise à niveau pour ceux qui ne connaissaient pas les technologies Angular et NodeJS, et pour présenter la structure du projet.

### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*
> Nous avons pu terminer 5 User stories sur 7, ce qui représente 13 / 17 = 76% de complexité.

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*

> En tant qu'utilisateur je veux pouvoir accéder au site

> En tant qu'utilisateur je veux pouvoir accéder à un ensemble des questions du quiz

>En tant qu'utilisateur j'accède à la page d'authentification pour me connecter

>En tant qu'utilisateur je veux pouvoir accéder à un choix de réponses possibles

>En tant qu'utilisateur je veux voir une question du quiz

## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
> Positif :\
    -Michel travaille très bien\
    -Provencal travaille tres bien\
    -Réussi à fournir une démo propre et utilisable\
\
Négatif :\
-Taches dépendantes : Certains devaient attendre qu'un autre finisse\
    -Difficulté sur la mise en place et l'installation du projet\
    -Répartition des tâches : Certains n'avaient pas de tâches\
    -User Story pas assez clair\
    -Mauvaise estimation des points\
    -2 Tâches n'ont pas pu être terminées

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*
 
### Axes d'améliorations 
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
>   -Plus de daily meeting\
    -Choisir des tâches indépendantes\
    -être fixé sur la répartition des premières user stories du sprint en cours.
    

## Prévisions de l'itération suivante  
### Évènements prévus  
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*
> Grand Sprint de 1 mois, donc plus de user Story prévu

### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
> -En tant qu'utilisateur je veux m'identifier pour accéder au quiz

> -En tant qu'utilisateur je veux pouvoir répondre à la question et  voir la bonne réponse

### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
> Voir Trello

## Confiance 
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 5 	|  *0* 	|  *0* 	    |  *5* 	|  *2* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 5 	|  *0* 	|  *0* 	    |  *0* 	|  *7* 	|

