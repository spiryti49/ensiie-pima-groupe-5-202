import { Component, Input, OnInit } from '@angular/core';
import { QuizzAPIService } from 'src/services/QuizzAPI.service';
import { PlayerScore } from '../types';
import { Router } from '@angular/router';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.scss']
})
export class ScoreboardComponent implements OnInit {

  @Input() players: PlayerScore[] = null;

  constructor(private quizzAPIService: QuizzAPIService, private router: Router) { }

  ngOnInit(): void {
    if(!this.players){
      this.quizzAPIService.getScoreboard().subscribe(scores => {
        this.players = scores;
      });
    }
  }

}