import { Component, OnInit,Input } from '@angular/core';
import { QuizzAPIService } from '../../services/QuizzAPI.service';
import { AuthService } from '../../services/Auth.service';


@Component({
  selector: 'app-page-score-solo',
  templateUrl: './page-score-solo.component.html',
  styleUrls: ['./page-score-solo.component.scss']
})
export class PageScoreSoloComponent implements OnInit {

  constructor(private apiservice:QuizzAPIService, private authService :AuthService) { }

  @Input() score:number=0;
  @Input() is20Question:boolean;
  public pseudo:string='Vroum';
  public comment:string;

  ngOnInit(): void {
      let divis:number= this.is20Question ? 1 : 10;
      this.pseudo = this.authService.getUsername();
      this.apiservice.sendScore({username: this.pseudo, score: this.score}).subscribe();
      if (this.score >= 190/divis){
        this.comment = "Vous êtes un dieu du QuIIzzE !!!"
      }
      else if (this.score >= 150/divis){
        this.comment = "Vous êtes vraiment très fort !"
      }
      else if (this.score >= 100/divis){
        this.comment = "C'est bien mais vous pouvez faire mieux!"
      }
      else {
        this.comment = "Au moins vous avez essayé..."
      }
  }

}
