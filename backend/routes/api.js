const express = require('express');
const router = express.Router();

const database = require('../database/database');

/**
 * Renvoie une question et 4 réponses possibles choisi aléatoirement dans la base de données et ordonnées aléatoirement
 */
router.post('/question', (req, res) => {
   database.getRandomQuestionReponses((question)=>{
      res.status(200).json(question);
   }, 
   ()=>{res.sendStatus(500);
   },req.body.Questiontab);
});
/**
 * renvoie la bonne réponse correspondant à l'idQuestion du body
 */
router.post('/answer',(req,res)=> {
   console.log(req.body)
   if(req.body.idQuestion==undefined){
      res.sendStatus(500);
   }
   else{
      database.getCorrectAnswer(req.body.idQuestion, (reponse)=>{
         res.status(200).json(reponse);
      }, ()=>{res.sendStatus(500)});
   }
})

/**
 * Renvoi le scoreboard, liste des pseudo avec leur score, triée du meilleur score au moins bon
 */
router.get('/scoreboard', (req, res) => {
   database.getScoreboard((resultat)=>{res.status(200).json(resultat)}, ()=>{res.sendStatus(500);});
});

/** 
 * Modifie un score dans la base de donnée pour un jouer :
 * Si le joueur n'existe pas encore, alors il est ajouté
 * Si le score est inférieur au score présent dans la base de donnée pour ce joueur, alors il n'est pas modifié
*/
router.post('/scoreboard', (req, res) => {
   const username = req.body.username;
   const score = req.body.score;

   /** 
    * TODO : Check si le score est valide
    * Check si le username est valide
   */
  
   database.setScore(username, score, ()=>{res.status(200).json();}, ()=>{res.sendStatus(500);});
});

/**
 * Ajoute une citaion dans la base de donnée
 * Si la personne citée n'existe pas encore, alors elle est ajoutée
 */
router.post('/add-quote', (req, res) => { 
   //Donnés à ajouter ou non dans la bdd
   const quote = req.body.quote;
   const answer = req.body.answer;


   database.addQuote(quote, answer, ()=>{res.status(200).json()}, ()=>{res.sendStatus(500)});

});

module.exports = router;