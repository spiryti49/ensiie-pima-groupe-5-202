DROP TABLE IF EXISTS Question;
DROP TABLE IF EXISTS Reponse;
DROP TABLE IF EXISTS Scoreboard;

CREATE TABLE Reponse (
    idReponse INT UNSIGNED NOT NULL AUTO_INCREMENT,
    reponse varchar(30),
    PRIMARY KEY (idReponse)
);

CREATE TABLE Question (
    idQuestion INT UNSIGNED NOT NULL AUTO_INCREMENT,
    question text,
    idQuestionReponse INT UNSIGNED NOT NULL,
    PRIMARY KEY (idQuestion),
    FOREIGN KEY (idQuestionReponse) REFERENCES Reponse(idReponse)
);

CREATE TABLE Scoreboard(
    username varchar(20) NOT NULL,
    score INT UNSIGNED NOT NULL,
    PRIMARY KEY (username)
);