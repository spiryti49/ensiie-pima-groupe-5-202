

const mysqlConf = require('./mysql_conf.json');

var mysql = require('mysql');
const { appendFileSync } = require('fs');
var connection = mysql.createPool({
    connectionLimit:200,
    host: mysqlConf.host,
    user: mysqlConf.user,
    password: mysqlConf.password,
    database: mysqlConf.database,
});




class Database {
    constructor() {
        console.log("Singleton ok");
    }

    /**
     * Renvoie une question et 4 réponses possibles choisi aléatoirement dans la base de données et ordonnées aléatoirement
     */
    getRandomQuestionReponses(success_cb, error_cb,tabReponse) {
        var builder = new String();
        if (tabReponse==undefined){
            this.sendQuery('SELECT * FROM `Question` ORDER BY RAND() LIMIT 1', (rows) => {
                this.sendQuery('(SELECT * FROM `Reponse` WHERE(`idReponse`!=' + rows[0].idQuestionReponse + ') ORDER BY RAND() LIMIT 3) UNION (SELECT * FROM `Reponse` WHERE(`idReponse`=' + rows[0].idQuestionReponse + ')) ORDER BY RAND()',
                    (rows2) => {
                        const obj = { question: { idQuestion: rows[0].idQuestion, question: rows[0].question }, reponses: rows2 };
                        if (success_cb) {
                            success_cb(obj);
                        }
                    }
                    , error_cb);
            }, error_cb);
        }
        else {
            for( var i= 0; i < tabReponse.length; i++ ) {
                builder.append(" ?, " );
              }
            this.sendQuery('SELECT * FROM `Question` WHERE idQuestion NOT IN ('+ builder.toString() +') ORDER BY RAND() LIMIT 1', (rows) => {
                this.sendQuery('(SELECT * FROM `Reponse` WHERE(`idReponse`!=' + rows[0].idQuestionReponse + ') LIMIT 3) UNION (SELECT * FROM `Reponse` WHERE(`idReponse`=' + rows[0].idQuestionReponse + ')) ORDER BY RAND()',
                    (rows2) => {
                        const obj = { question: { idQuestion: rows[0].idQuestion, question: rows[0].question }, reponses: rows2 };
                        if (success_cb) {
                            success_cb(obj);
                        }
                    }
                    , error_cb);
            }, error_cb);
        }
    }

    /**
    * renvoie la bonne réponse correspondant à idQuestion
    */
    getCorrectAnswer(idQuestion, success_cb, error_cb) {
        this.sendQuery("SELECT `idQuestionReponse` FROM `Question` WHERE `idQuestion`=" + idQuestion, (rows) => {
            if (success_cb) {
                success_cb({ idReponse: rows[0].idQuestionReponse });
            }
        }, error_cb);
    }

    /**
     * Renvoi le scoreboard, liste des pseudo avec leur score, triée du meilleur score au moins bon
     */
    getScoreboard(success_cb, error_cb) {
        this.sendQuery("SELECT * FROM Scoreboard ORDER BY score DESC", (resultat) => {
            if (success_cb) {
                success_cb(resultat);
            }
        }, error_cb);
    }

    /** 
      * Modifie un score dans la base de donnée pour un jouer :
      * Si le joueur n'existe pas encore, alors il est ajouté
      * Si le score est inférieur au score présent dans la base de donnée pour ce joueur, alors il n'est pas modifié
     */
    setScore(username, score, success_cb, error_cb) {
        this.sendQuery(`SELECT * from Scoreboard WHERE username = '${username}'`, (rows) => {
            /**Si il n'y a aucun score pour ce pseudo, on l'ajoute */
            if (rows.length === 0) {
                this.sendQuery(`INSERT INTO Scoreboard VALUES ('${username}', ${score})`, success_cb, error_cb);
            } else if (rows[0].score < score) { /**Le score existe déjà mais est inférieur, on l'update */
                this.sendQuery(`UPDATE Scoreboard SET score = ${score} WHERE username = '${username}'`, success_cb, error_cb);
            } else if (success_cb) { /**Si le score est inf on ne fait rien */
                success_cb();
            }
        }, error_cb);
    }

    addQuote(quote, answer, success_cb, error_cb) {
        this.sendQuery(`SELECT * from Reponse WHERE reponse LIKE "%${answer}%"`, (rows) => {
            //La personne citée n'exite pas 
            if (rows.length === 0) {
                this.sendQuery(`INSERT INTO Reponse (reponse) VALUES ("${answer}")`,(resultat)=>{
                    this.sendQuery(`INSERT INTO Question (question, idQuestionReponse) VALUES ("${quote}", ${resultat.insertId})`, success_cb, error_cb);
                } , error_cb);
            } else { //La personne citée existe déjà 
                this.sendQuery(`INSERT INTO Question (question, idQuestionReponse) VALUES ("${quote}", ${rows[0].idReponse})`, success_cb, error_cb);
            }
        }, error_cb);
    }

    sendQuery(query, success_cb, error_cb) {
        connection.query(query, (err, resultat) => {
            if (err) {
                if (error_cb) {
                    console.log(query);
                    console.log(err);
                    error_cb();
                }
            } else {
                if (success_cb) {
                    success_cb(resultat);
                }
            }
        });
    }
}

module.exports = new Database();