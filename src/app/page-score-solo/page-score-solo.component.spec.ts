import { ComponentFixture, TestBed } from '@angular/core/testing';


import { PageScoreSoloComponent } from './page-score-solo.component';

describe('PageScoreSoloComponent', () => {
  let component: PageScoreSoloComponent;
  let fixture: ComponentFixture<PageScoreSoloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageScoreSoloComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageScoreSoloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
