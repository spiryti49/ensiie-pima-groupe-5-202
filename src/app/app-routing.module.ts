import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormulaireDeQuestionComponent } from './formulaire-de-question/formulaire-de-question.component';
import {AuthentificationPageComponent} from './authentification-page/authentification-page.component';
import { ScoreboardComponent } from './scoreboard/scoreboard.component';
import { AddQuoteComponent } from './add-quote/add-quote.component';
import { MenuComponent } from './menu/menu.component';
import { MultiplayerRoomComponent } from './multiplayer-room/multiplayer-room.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'question', component:FormulaireDeQuestionComponent,
  }, 
  {
    path: 'authentification', component:AuthentificationPageComponent,
  },
  {
    path: 'scoreboard', component:ScoreboardComponent,  
  },
  {
    path: 'add-quote', component:AddQuoteComponent,
  },
  {
    path: 'room/:id', component:MultiplayerRoomComponent,
  }
  ,
  {
    path: 'room', component:MultiplayerRoomComponent,
  },
  {
    path: 'home', component:HomeComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
