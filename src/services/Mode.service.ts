import { Injectable } from "@angular/core";

export enum Mode{
    question_20,
    time_limite
};
@Injectable({
    providedIn: 'root'
})

export class ModeService {


    private mode:Mode=null;

    public setMode(mode:Mode){
        this.mode=mode;
    }
    public getMode(){
        return this.mode;
    }

}