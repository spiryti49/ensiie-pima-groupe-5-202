import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormulaireDeQuestionComponent } from './formulaire-de-question/formulaire-de-question.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthentificationPageComponent } from './authentification-page/authentification-page.component';
import { ScoreboardComponent } from './scoreboard/scoreboard.component';
import { HeaderComponent } from './header/header.component';
import { AddQuoteComponent } from './add-quote/add-quote.component';
import { MenuComponent } from './menu/menu.component';
import { MultiplayerRoomComponent } from './multiplayer-room/multiplayer-room.component';
import { IconModule } from "@visurel/iconify-angular";
import { ReponseComponent } from './reponse/reponse.component';
import { PageScoreSoloComponent } from './page-score-solo/page-score-solo.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    FormulaireDeQuestionComponent,
    AuthentificationPageComponent,
    ScoreboardComponent,
    HeaderComponent,
    AddQuoteComponent,
    MenuComponent,
    MultiplayerRoomComponent,
    ReponseComponent,
    PageScoreSoloComponent,
    HomeComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    IconModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
