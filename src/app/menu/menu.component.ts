import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/services/Auth.service';

// Créer une page d'accueil où on explique les règles
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(private router: Router, private authService: AuthService) { } 

  public isOnLoginScreen: boolean = false;
  public isLoggedIn: boolean = false;
  public pageButton: number[] = [0,0,0,0,0,0,0];

  ngOnInit(): void {
    this.router.events.subscribe((event:any) => {
      if(event.url){
        const url = event.url;
        switch(url){
          case '/home': this.pageButton = [1,0,0,0,0,0,0]; break;
          case '/question': this.pageButton = [0,1,0,0,0,0,0];break;
          case '/question?timer_mode=1': this.pageButton = [0,0,1,0,0,0,0];break;
          case '/scoreboard': this.pageButton = [0,0,0,1,0,0,0];break;
          case '/add-quote': this.pageButton = [0,0,0,0,0,1,0];break;
          case '/authentification':this.pageButton = [0,0,0,0,0,0,1]; break;
          default: this.pageButton = [0,0,0,0,0,0,0];
        }

      }
    })
    this.authService.onLog.subscribe((username) => {
      this.isLoggedIn = username !== null; 
    });
    this.isOnLoginScreen = this.router.url === '/authentification';
  }

  /**On redirige vers la page d'accueil */
  buttonHome(){
    this.router.navigate(['home']);
  }

  /**On redirige vers le mode solo avec nbr question limité */
  buttonQuestionSolo(){
    /**On redirige vers les questions */
    this.router.navigate(['question']);
  }

  /**On redirige vers le mode solo avec temps limité */
  buttonQuestionSolo2(){
    this.router.navigate(['question'], { queryParams: { timer_mode: 1 }});
  }

  /**On redirige vers le tableau des scores */
  buttonScore(){
    this.router.navigate(['scoreboard']);
  }

  /**On redirige vers le mode multijoueur */
  redirectRoom(){
    this.router.navigate(['room']);
  }

  /**On redirige vers la page pour ajouter une citation */
  buttonAddQuote(){
    this.router.navigate(['add-quote']);
  }
  /**On redirige vers la page d'authentification et on quitte la session */
  buttonExit(){
    this.authService.logout();
    this.router.navigate(['authentification']);
  }
}
