import { Injectable } from "@angular/core";
import { webSocket, WebSocketSubject } from "rxjs/webSocket";
import { Subject } from 'rxjs';
import { AuthService } from "./Auth.service";
import { RoomState } from "src/app/types";
@Injectable({
    providedIn: 'root'
})
/**
 * Service / Interface qui gere les websocket pour le multiplayer
 */
export class MultiplayerService {

    /* Websocket vers le serveur*/
    private socket: WebSocketSubject<any>;
    /* ID du joueur actuel */
    private id?: string;


    /**
     * Observable, déclenché quand le joueur actuel rejoint une room
     * room: id de la room
     */
    public onJoiningRoom = new Subject();

    /** 
     * Observable, déclenché quand des joueurs entrent, ou qu'un joueur quitte la partie
     * user_join: une liste de joueur (id, username)
     * user_left: un id d'utilisateur
    */
    public onUserChange = new Subject();

    /**
     * Observable, déclenché quand l'admin de la partie change
     * admin: id du nouvel admin
     */
    public onAdminChange = new Subject();

    /** 
     * Observable, déclenché quand la configuration de la room change
     * max_score: nouveau score maximum à atteindre
     */
    public onConfigurationChange = new Subject();

    /**
     * Observable, déclenché quand l'état de la room change
     * state : "start", "end"
     */
    public onStateChange = new Subject();

    /**
     * Observable, déclenché quand une nouvelle question est reçue
     * question:{question:{question:string, idQuestion:number}, reponses:[{reponse:string, idReponse:number}]}
     */
    public onNewQuestion = new Subject();

    /**
     * Observable, déclenché quand un joueur à envoyé une réponse
     * id du joueur
     */
    public onPlayerGuessed = new Subject();

    /**
     * Observable, déclenché quand la bonne réponse est reçue
     * id de la bonne réponse
     */
    public onReponse = new Subject();

    /**
     * Observable, déclenché quand le scoreboard est recu
     * [{player:string, score:number}]
     */
    public onScoreboard = new Subject();

    /**
     * Observable, déclenché quand on recoit les réponses de tout le monde
     * [player:string, reponse:number]
     */
    public onAllGuess = new Subject();


    constructor(private authService: AuthService) {

        this.authService.onLog.subscribe(username=>{
            if(!username){
            }
        })
    }

    /**
     * Logout
     */
    public logout(): void {
        if(this.socket){
            this.socket.complete();
        }
        this.socket = null;
        this.id = null;
    }

    /**
     * Getter, id du joueur actuel
     * @returns ID (string) du joueur actuel
     */
    public getPlayerID(): string {
        return this.id;
    }

    /**
     * Connecte l'utilisateur au serveur (websocket) multijoueur
     */
    public connect(): void {
        const hostname = window.location.hostname;
        const webSocketProtocol = hostname === 'localhost' ? 'ws' : 'wss'; /** Si on est en localhost => ws, si on est sur anunes.iiens.net => wss*/
        /**L'utilisateur doit être connecté pour jouer en multi */
        if (this.authService.isAuth()) {
            this.socket = webSocket(`${webSocketProtocol}://${hostname}:6743`); /**Connection au serveur multijoueur (websocket) */
            this.socket.subscribe(message => { this.handleMessage(message); }); /**Les messages que le serveur envoit au client, seront traités par la methode handleMessage  */
            this.login(); /** Envoi du pseudo  */
        }
    }

    /**
     * Demande à rejoindre une room
     * @param roomID (string) ID de la room à rejoindre
     */
    requestRoom(roomID: string): void {
        this.sendMessage({ room_request: roomID });
    }

    /**
     * Demande à changer les parametres de la room (seulement pour l'admin de la room)
     * @param maxScore (number) nouveau score max à atteindre
     */
    changeRoomConfiguration(maxScore):void {
        this.sendMessage({ room_configuration: { max_score: maxScore } });
    }

    /**
     * Demande de lancer la partie
     */
    startRoom(): void {
        this.sendMessage({start:true});
    }

    /**
     * Demande de relancer la partie
     */
    restartGame(): void {
        this.sendMessage({restart:true});
    }

    /**
     * Envoi d'une réponse au serveur
     * @param idReponse ID de la réponse envoyé
     */
    guess(idReponse) : void {
        this.sendMessage({guess:idReponse});
    }

    /**
    * Envoi le pseudo au serveur multijoueur
    */
    private login(): void {
        this.sendMessage({ username: this.authService.getUsername() });
    }

    /**
     * Envoi un message au serveur multijoueur
     * @param message (JSON) le message à envoyer
     */
    sendMessage(message: any): void {
        this.socket.next(message);
    }

    /**
     * Fonction qui gère les messages reçu
     * @param message (JSON) message reçu
     */ 
    handleMessage(message: any): void {
        console.log(message);
        if (message.id) { this.id = message.id; }
        if (message.room) { this.onJoiningRoom.next(message.room); }
        if (message.user_join) { this.onUserChange.next({ user_join: message.user_join }); }
        if (message.user_left) { this.onUserChange.next({ user_left: message.user_left }); }
        if (message.admin) { this.onAdminChange.next({ admin: message.admin }); }
        if (message.room_configuration) { this.onConfigurationChange.next(message.room_configuration); }
        if (message.question) { this.onNewQuestion.next(message.question); }
        if (message.player_guessed) { this.onPlayerGuessed.next(message.player_guessed); }
        if (message.reponse) { this.onReponse.next(message.reponse); }
        if (message.scoreboard) {this.onScoreboard.next(message.scoreboard);}
        if (message.all_guess) {this.onAllGuess.next(message.all_guess);}
        if (message.room_state) { 
            const state : RoomState = (<any>RoomState)[message.room_state];
            this.onStateChange.next(state);
        }

    }
    


}