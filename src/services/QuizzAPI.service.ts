import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PlayerScore, Question, QuotetoAdd, Reponse } from '../app/types';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuizzAPIService {

  constructor(private http: HttpClient) {}

  /**
   * récupère une question aléatoire et 4 réponses possibles
   *  @returns {question:Question,reponses:Reponse[]}
   * 
   *   */ 

  
  getQuestion(Questiontab:number[]){
    return this.http.post<{question:Question,reponses:Reponse[]}>('/api/question',Questiontab);
  }
  
  sendReponse(idQuestion:number,idReponse:number){
    return this.http.post<{idReponse:number}>('/api/answer',{idQuestion:idQuestion,idReponse:idReponse})
  }


  /**
   * Renvoi la liste des scores
   * @returns Observable, d'une liste de score
   */
  getScoreboard(): Observable<PlayerScore[]>{
    return this.http.get<PlayerScore[]>('/api/scoreboard');
  }

  /**
   * Send a new player score ({username, score}), to the api
   * @param playerScore The player score to send
   * @returns Observable
   */
  sendScore(playerScore: PlayerScore){
    return this.http.post<PlayerScore>('/api/scoreboard', playerScore);
  }

  /**
   * Ajoute une citation 
   * @param quote Un QuotetoAdd contenant la citation et pas personne citée
   * @returns Observable
   */
  sendQuote(quote: QuotetoAdd) { /**TODO : ajouter la réponse */
    return this.http.post<QuotetoAdd>('/api/add-quote',quote);
  }
}
