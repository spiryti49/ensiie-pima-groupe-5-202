import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";

@Injectable({
    providedIn: 'root'
})

export class AuthService {
    private auth:boolean;
    private username:string;
    public onLog:Subject<string>;

    constructor(){
        this.auth = false;
        this.username = "???";
        this.onLog = new Subject();
    }

    /**
     * Retourne True si le joueur est connecté, faux sinon 
     */
    isAuth() : boolean{
        return this.auth;
    }

    /**
     * Déconnecte le joueur
     */
    logout(){
        this.auth = false;
        this.onLog.next(null);
    }

    /**
     * Connecte le joueur
     * @param username Pseudo du joueur
     */
    login(username:string){
        this.username = username;
        this.auth = true;
        this.onLog.next(this.username);
    }

    /**
     * Retourne le pseudo du joueur connecté
     *  
     */
    getUsername(){
        return this.username;
    }

}