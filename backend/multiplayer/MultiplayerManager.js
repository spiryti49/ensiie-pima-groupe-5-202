const WebSocket = require('ws');
const fs = require('fs');

let https = null;
let cert = {}

const port = 6743;

const Player = require('./Player');
const Room = require('./Room');

/**Si on est sur le serveur nunesa.iiens.net (prod) on lance les websocket en TLS (sinon ça passe pas) */
if(process.env.QUIIZE_PROD){
    https = require('https');
    cert = {
        key: fs.readFileSync('../.cert/privkey.pem'),
        cert: fs.readFileSync('../.cert/fullchain.pem')      
    };
    console.log('[WEBSOCKET] => HTTPS');
}else{
    https = require('http');
    console.log('[WEBSOCKET] => HTTP');

}


/**
 * Cette classe gère le serveur WebSocket pour la partie Multiplayer 
 * et fait le lien pour trouver une room par son id
 */
class MultiPlayerManager {
    constructor() {
        this.rooms = new Map(); /**Map des Room (IDroom => Room) */
    }

    /**
     * Lance le serveur websocket pour le multiplayer
     */
    start() {
        const server = https.createServer(cert);

        server.listen(port, ()=>{
            console.log(`Server for Websocket on ${port}`);
        });
        this.wss = new WebSocket.Server({server:server });

        this.wss.on('listening', () => { console.log("[Multiplayer] WebSocketServer running...") });
        this.wss.on('error', (error) => { console.error("[Multiplayer] WebSocketServer error:", error) });

        /**A chaque nouvelles connexions au serveur websocket */
        this.wss.on('connection', (socketClient) => {
            this.handleNewSocket(socketClient);
        });
    }

    /**
     * Essaye d'ajouter un joueur à une room, par son ID, si la room n'existe pas elle est créée
     * @param {*} player Player qui souhaite rejoindre une room
     * @param {*} roomID ID de la room (créée si n'existe pas)
     */
    joinRoom(player, roomID) {
        let room = this.rooms.get(roomID);
        /**On créer la room si elle n'existe pas */
        if (!room) {
            room = new Room(roomID, this);
            this.rooms.set(roomID, room);
        }

        room.join(player);
    }

    /**
     * Retire une room
     * @param {*} roomID L'id de la room à fermer 
     */
    removeRoom(roomID) {
        this.rooms.delete(roomID);
    }

    /**
     * Nouvelle connexion au servur websocket, on créer un nouveau Player
     * @param {*} socket socket du client
     */
    handleNewSocket(socket) {
        new Player(socket, this);
    }
}


module.exports = new MultiPlayerManager();