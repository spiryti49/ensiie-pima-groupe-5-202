
const Utils = require('../Utils');

/**
 * Représente un joueur en MultiJoueur
 * Cette classe permet d'envoyer des messages au joueur et gère les messages envoyé par le joueur au serveur
 */
class Player {
    constructor(socket, multiplayerManager) {
        this.room = null; /** Object Room, au quel le joueur appartient */
        this.username = null; /** Username du joueur */
        this.id = Utils.randomID(16); /** ID du joueur */
        this.score = 0; /**Score du joueur */

        this.multiplayerManager = multiplayerManager;
        this.socket = socket;

        this.send({ id: this.id }); /**On envoit son id au joueur (peut être supprimer ça, ça sert à rien) */

        /**A chaque fois qu'on recoit un message du joueur, on execute handleMessage */
        this.socket.on('message', message => {
            this.handleMessage(message);
        });
        this.socket.on('close', () => {
            this.destroy(); /**Si la socket se ferme, on détruit le joueur */
        });

        this.socket.on('error', error => { console.error(`[Error][Player] ${this.username} : ${error} `) });

    }

    /**
     * Demande à rejoindre une room
     * @param {*} id ID de la room que le joueur veut rejoindre
     */
    joinRoom(id) {
        this.multiplayerManager.joinRoom(this, id);
    }

    /**
     * Set la room du joueur
     * @param {*} room 
     */
    setRoom(room) {
        this.room = room;
    }

    /**
     * Set le username
     * @param {*} username 
     */
    login(username) {
        let valid = /^([A-z0-9_]){3,15}$/.test(username); /**Regexp : Combinaison de chiffres, lettres, _, de taille >=3 && <= 15 */
        if(valid){
            this.username = username;
        }else{
            this.destroy();
        }
    }


    /**
     * Gestion des messages recu 
     * @param {*} message 
     */
    handleMessage(message) {
        try {
            const data = JSON.parse(message);

            if (data.username) this.login(data.username); /**Type 'username' */
            
            if (this.username && data.room_request) this.joinRoom(data.room_request); /** Demande de rejoindre une room */
            if(this.room){
                if (data.room_configuration ) this.room.changeConfiguration(this.id, data.room_configuration);
                if (data.start ) this.room.start(this.id);
                if (data.restart) this.room.restart(this.id);
                if(data.guess) this.room.guess(this.id, data.guess);
            }

            console.log(`${this.id}:${this.username} : ${message}`);
        } catch (e) {/** Si le message n'est pas JSON/ Error : On ferme la connection */
            console.log('[Multiplayer] User error : ', e);
            this.destroy();
        }
    }

    /**
     * Envoi un message (JSON) au joueur
     * @param {*} message 
     */
    send(message) {
        if (this.socket.readyState === 1) {
            this.socket.send(JSON.stringify(message));
        } else {
            this.destroy();
        }
    }

    /**
     * Détruit le player
     */
    destroy() {
        if (this.room) this.room.left(this.id); /**Si il est dans une room, on le fait quitter */
        this.multiplayerManager = null;
        this.socket = null;
        this.id = null;
        this.room = null;
    }


}

module.exports = Player;